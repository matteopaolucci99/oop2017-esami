package a01a.e1;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class InfiniteSequenceOpsImpl implements InfiniteSequenceOps {

	@Override
	public <X> InfiniteSequence<X> ofValue(X x) {
		return () -> x;
	}

	@Override
	public <X> InfiniteSequence<X> ofValues(List<X> l) {
		return new InfiniteSequence<X>() {
			int current = 0;

			@Override
			public X nextElement() {
				if(current == l.size()) {
					current = 0;
				} 
				return l.get(current++);
			}
		};
	}

	@Override
	public InfiniteSequence<Double> averageOnInterval(InfiniteSequence<Double> iseq, int intervalSize) {
		return new InfiniteSequence<Double>() {
			private List<Double> current = new LinkedList<>();
			
			@Override
			public Double nextElement() {
				for(int i = 0; i < intervalSize; i++) {
					current.add(iseq.nextElement());
				}
				double avarage = current.stream().mapToDouble(x -> x).average().getAsDouble();
				current.clear();
				return avarage;
			}
		};
	}

	@Override
	public <X> InfiniteSequence<X> oneEachInterval(InfiniteSequence<X> iseq, int intervalSize) {
		return () -> Stream.generate(() -> iseq.nextElement()).skip(intervalSize -1).findFirst().get(); 
	}

	@Override
	public <X> InfiniteSequence<Boolean> equalsTwoByTwo(InfiniteSequence<X> iseq) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <X, Y extends X> InfiniteSequence<Boolean> equalsOnEachElement(InfiniteSequence<X> isx,
			InfiniteSequence<Y> isy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <X> Iterator<X> toIterator(InfiniteSequence<X> iseq) {
		return new Iterator<X>() {

			@Override
			public boolean hasNext() {
				return true;
			}

			@Override
			public X next() {
				return iseq.nextElement();
			}
		};
	}

}
