package a01b.e1;

import java.util.Iterator;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class TraceFactoryImpl implements TraceFactory {

	@Override
	public <X> Trace<X> fromSuppliers(Supplier<Integer> sdeltaTime, Supplier<X> svalue, int size) {
		final Iterator<Integer> iterator = Stream.iterate(0, i -> i + sdeltaTime.get()).iterator();
		final Stream<Event<X>> stream = Stream.generate(() -> (Event<X>) new EventImpl<X>(iterator.next(), svalue.get())).limit(size);
		return new TraceImpl<X>(stream);
	}

	@Override
	public <X> Trace<X> constant(Supplier<Integer> sdeltaTime, X value, int size) {
		final Iterator<Integer> iterator = Stream.iterate(0, i -> i + sdeltaTime.get()).iterator();
		final Stream<Event<X>> stream = Stream.generate(() -> (Event<X>) new EventImpl<X>(iterator.next(), value)).limit(size);
		return new TraceImpl<X>(stream);
	}

	@Override
	public <X> Trace<X> discrete(Supplier<X> svalue, int size) {
		final Iterator<Integer> iterator = Stream.iterate(0, i -> i+1).iterator();
		final Stream<Event<X>> stream = Stream.generate(() -> (Event<X>) new EventImpl<X>(iterator.next(), svalue.get())).limit(size);
		return new TraceImpl<X>(stream);
	}

}
