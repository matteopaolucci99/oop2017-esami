package a01b.e1;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TraceImpl<X> implements Trace<X> {
	private List<Event<X>> list;
	private Iterator<Event<X>> iterator;
	
	public TraceImpl(Stream<Event<X>> stream) {
		this.list = new LinkedList<>(stream.collect(Collectors.toList()));
		this.iterator = this.list.iterator();
	}

	@Override
	public Optional<Event<X>> nextEvent() {
		return this.iterator.hasNext() ? Optional.of(this.iterator.next()) : Optional.empty();
	}

	@Override
	public Iterator<Event<X>> iterator() {
		return this.iterator;
	}

	@Override
	public void skipAfter(int time) {
		for(int i = 0; i < time; i++) {
			this.nextEvent();
		}
	}

	@Override
	public Trace<X> combineWith(Trace<X> trace) {
		Iterator<Event<X>> it = trace.iterator();
		List<Event<X>> l = new LinkedList<Event<X>>(this.list);
		while(it.hasNext()) {
			l.add(it.next());
		}
		l.sort((e1, e2) -> Integer.compare(e1.getTime(), e2.getTime()));
		return new TraceImpl<>(l.stream());
	}

	@Override 
	public Trace<X> dropValues(X value) {
		Stream<Event<X>> tmp = new LinkedList<Event<X>>(this.list).stream();
		return new TraceImpl<X>(tmp.filter(e -> !e.getValue().equals(value)));
	}
}
