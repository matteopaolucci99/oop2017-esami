package a02b.e1;

import java.util.*;

public abstract class AbstractTransducer<X, Y> implements Transducer<X, Y> {
	private Queue<X> inputs;
	private Queue<Y> outputs;
	private boolean inputIsOver;
	
	public AbstractTransducer() {
		this.inputIsOver = false;
		this.inputs = new LinkedList<>();
		this.outputs = new LinkedList<>();
	}	

	@Override
	public void provideNextInput(X x) {
		Queue<Y> tmp = new LinkedList<>(outputs);
		if(this.inputIsOver) {
			throw new IllegalStateException();	
		}
		this.inputs.add(x);
		this.computeOutput();
		if(!tmp.equals(outputs)) {
			this.inputs.clear();
		}
	}

	protected abstract void computeOutput();


	@Override
	public void inputIsOver() {
		if (this.inputIsOver) {
			throw new IllegalStateException();
		}		
		this.inputIsOver = true;		
		if(!this.inputs.isEmpty()) {
			this.computeOutput();
		}
	}

	@Override
	public boolean isNextOutputReady() {
		return !this.outputs.isEmpty();
	}

	@Override
	public Y getOutputElement() {
		if(this.outputs.isEmpty()) {
			throw new IllegalStateException();
		}
		return this.outputs.remove();
	}

	@Override
	public boolean isOutputOver() {
		return this.inputIsOver && this.outputs.isEmpty();
	}

	protected void appendOutputs(Y elem) {
		this.outputs.add(elem);
	}

	protected Queue<X> getInputs() {
		return inputs;
	}
	
	protected boolean getInputIsOver() {
		return this.inputIsOver;
	}
}
