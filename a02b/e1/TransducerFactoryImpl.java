package a02b.e1;

public class TransducerFactoryImpl implements TransducerFactory {

	@Override
	public <X> Transducer<X, String> makeConcatenator(int inputSize) {
		return new AbstractTransducer<X, String>() {
			@Override
			protected void computeOutput() {
				if (this.getInputs().size() == inputSize) {
					this.appendOutputs(
							this.getInputs().stream().map(X::toString).reduce((s1, s2) -> s1.concat(s2)).get());
				} else if (this.getInputIsOver()) {
					for (X val : this.getInputs()) {
						this.appendOutputs(val.toString());
					}
				}
			}
		};
	}

	@Override
	public Transducer<Integer, Integer> makePairSummer() {
		return new AbstractTransducer<Integer, Integer>() {
			@Override
			protected void computeOutput() {
				if (this.getInputs().size() == 2) {
					this.appendOutputs(this.getInputs().stream().mapToInt(x -> x).sum());
				} else if (this.getInputIsOver()) {
					for (int val : this.getInputs()) {
						this.appendOutputs(val);
					}
				}
			}
		};
	}

}
