package a02a.e1;

import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

public class SequenceAcceptorImpl implements SequenceAcceptor {
	static {
		funcs = new EnumMap<>(Sequence.class) {{
			this.put(Sequence.POWER2, i -> (int)Math.pow(2, i));
			this.put(Sequence.FLIP, i -> i % 2 == 0 ? 1 : 0);
			this.put(Sequence.FIBONACCI, i -> fibonacci(i));
			this.put(Sequence.RAMBLE, i -> i % 2 == 0 ? 0 :  (i + 1) / 2);
		}};		
	}
	
	private static Map<Sequence, Function<Integer, Integer>> funcs;
	private Sequence sequence;	
	private int iteration;

	@Override
	public void reset(Sequence sequence) {
		this.sequence = sequence;
		this.reset();
	}

	@Override
	public void reset() {
		check(this.sequence);
		this.iteration = 0;
	}

	@Override
	public void acceptElement(int i) {
		check(this.sequence);
		if (i != funcs.get(this.sequence).apply(iteration)) {
			throw new IllegalStateException();
		}
		this.iteration++;
	}
	
	private void check(Object obj) {
		if(Objects.isNull(obj)) {
			throw new IllegalStateException();
		}
	}

	private static int fibonacci(int iteration) {
		return iteration <= 1 ? 1 : fibonacci(iteration - 1) + fibonacci(iteration - 2);
	}
}
